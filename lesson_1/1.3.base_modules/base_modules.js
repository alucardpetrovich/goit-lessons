const fire = require('js-fire');
const fs = require('fs');
const path = require('path');
const assert = require('assert');
const http = require('http');
const querystring = require('querystring');

const examples = {
    fs: {
        writeFile: (filename, text) => {
            fs.writeFile(filename, text, (err) => {
                console.log(
                    `Text "${text}" was successfully written into newly`
                    + ` created file "${filename}"`
                );
            });
        }
    },
    path: {
        join: (path1, path2) => {
            console.log(`Pathes were joined into "${path.join(path1, path2)}"`);
        }
    },
    assert: {
        equals: (val1, val2) => {
            assert.equal(val1, val2);
            console.log('values are equal');
        }
    },
    http: {
        createServer: (port) => {
            http
                .createServer((req, res) => {
                    res.end('Hello client');
                })
                .listen(port, () => {
                    console.log(`Server listening on port ${port}`);
                });
        },
        createClient: (port) => {
            http.get(`http://localhost:${port}`, (res) => {
                let data = '';
                res.on('data', (chunk) => {
                    data += chunk.toString('utf8');
                }).on('end', () => {
                    console.log(data);
                });
            });
        }
    },
    querystring: {
        parse: (str) => {
            console.log(querystring.parse(str));
        }
    }

};

fire(examples);
