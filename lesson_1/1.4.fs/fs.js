const fire = require('js-fire');
const fs = require('fs');
const path = require('path');
const fsPromises = fs.promises;

const examples = {
    writeFile: (filename, text = '', flag = 'w') => {
        fs.writeFile(filename, text, { flag }, (err) => {
            if (err) throw err;
            console.log(`File "${filename}" was created with text "${text}"`);
        });
    },
    appendFile: (filename, text = '') => {
        fs.appendFile(filename, text, (err) => {
            if (err) throw err;
            console.log(`Text "${text}" was added to file "${filename}"`)
        });
    },
    readFile: (filename) => {
        fs.readFile(filename, 'utf8', (err, data) => {
            if (err) throw err;
            console.log(`File content: "${data}"`);
        });
    },
    unlink: (filename) => {
        fs.unlink(filename, (err) => {
            if (err) throw err;
            console.log(`File "${filename}" was removed`)
        });
    },
    rename: (filename, newFilename) => {
        fs.rename(filename, newFilename, (err) => {
            if (err) throw err;
            console.log(`File "${filename}" was renamed to "${newFilename}"`);
        });
    },
    mkdir: (dirname) => {
        fs.mkdir(dirname, (err) => {
            if (err) throw err;
            console.log(`Directory "${dirname}" created`);
        });
    },
    rmdir: (dirname) => {
        fs.rmdir(dirname, (err) => {
            if (err) throw err;
            console.log(`Directory "${dirname}" removed`);
        });
    },
    writeFilePromise: async (filename, text = '', flag = 'w') => {
        await fsPromises.writeFile(filename, text, { flag });
        console.log(`File "${filename}" was created with text "${text}"`);
    },
    unlinkPromise: async (filename) => {
        await fsPromises.unlink(filename);
        console.log(`File "${filename}" was deleted`);
    }
};

fire(examples);
